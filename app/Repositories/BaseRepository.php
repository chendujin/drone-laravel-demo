<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * The model to be used by the repository
     *
     * @var object
     */
    protected object $model;

    /**
     * The includes for the current query
     *
     * @var array
     */
    protected array $with = [];

    /**
     * BaseRepository constructor.
     *
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all rows
     *
     * @return Collection
     */
    public function all(): Collection
    {
        $response = $this->model->with($this->with)->get();
        $this->with = [];
        return $response;
    }

    /**
     * Get single row
     *
     * @param int $id The object ID.
     * @return Model
     */
    public function find(int $id): Model
    {
        $response = $this->model->with($this->with)->find($id);
        $this->with = [];
        return $response;
    }

    /**
     * Get single row, or throw a 404
     *
     * @param int $id The object ID.
     * @return Model
     */
    public function findOrFail(int $id): Model
    {
        $response = $this->model->with($this->with)->findOrFail($id);
        $this->with = [];
        return $response;
    }

    /**
     * Create row
     *
     * @param array $input The input data.
     * @return Model
     */
    public function create(array $input): Model
    {
        return $this->model->create($input);
    }

    /**
     * Update row
     *
     * @param int $id The object ID.
     * @param array $input The input data.
     * @return int
     */
    public function update(int $id, array $input): int
    {
        return $this->model->find($id)->update($input);
    }

    /**
     * Delete row
     *
     * @param int $id The object ID.
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * Get rows matching parameters
     *
     * @param array $parameters The parameters.
     * @param string|null $field The field.
     * @param string|null $order The order.
     * @return Collection
     */
    public function where(array $parameters, ?string $field = null, ?string $order = null): Collection
    {
        $query = $this->model->with($this->with)->where($parameters);
        if ($order) {
            $query->orderBy($field, $order);
        }
        $response = $query->get();
        $this->with = [];
        return $response;
    }

    /**
     * Get a single row matching parameters
     *
     * @param int $id The object ID.
     * @param array $parameters The parameters.
     * @return Collection
     */
    public function findWhere(int $id, array $parameters): Collection
    {
        $response = $this->model->with($this->with)->where($parameters)->find($id);
        $this->with = [];
        return $response;
    }

    /**
     * Get a single row matching parameters, or throw a 404
     *
     * @param int $id The object ID.
     * @param array $parameters The parameters.
     * @return Collection
     */
    public function findWhereOrFail(int $id, array $parameters): Collection
    {
        $response = $this->model->with($this->with)->where($parameters)->findOrFail($id);
        $this->with = [];
        return $response;
    }

    /**
     * Set includes
     *
     * @param array $tables The tables to include.
     * @return BaseRepository
     */
    public function with(array $tables): BaseRepository
    {
        $this->with = $tables;
        return $this;
    }

    /**
     * Get model name
     *
     * @return string
     */
    public function getModel(): string
    {
        return get_class($this->model);
    }

    /**
     * Get or create row
     *
     * @param array $input The input data.
     * @return Model
     */
    public function firstOrCreate(array $input): Model
    {
        return $this->model->firstOrCreate($input);
    }

    /**
     * Update or create row
     *
     * @param array $input The input data.
     * @return int|Model
     */
    public function updateOrCreate(array $input): Model|int
    {
        return $this->model->updateOrCreate($input);
    }

    /**
     * Attach a model
     *
     * @param mixed $model The first model.
     * @param string $relation The relationship on the first model.
     * @param Model $value The model to attach.
     * @return void
     */
    public function attach(mixed $model, string $relation, Model $value): void
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        $model->$relation()->attach($value);
    }

    /**
     * Detach a model
     *
     * @param mixed $model The first model.
     * @param string $relation The relationship on the first model.
     * @param Model $value The model to attach.
     * @return void
     */
    public function detach(mixed $model, string $relation, Model $value): void
    {
        if (!$model instanceof Model) {
            $model = $this->find($model);
        }

        $model->$relation()->detach($value);
    }
}
