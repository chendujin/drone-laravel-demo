<?php

namespace App\Constants\Http;

class FileExtensions
{
    public const GIF = 'gif';

    public const HTML = 'html';

    public const JSON = 'json';

    public const MHT = 'mht';

    public const MHTML = 'mhtml';

    public const TWIG = 'twig';

    public const XML = 'xml';

    public const JPG = 'jpg';

    public const JPEG = 'jpeg';

    public const PNG = 'png';
}
