<?php

namespace App\Models;

use Exception;
use DateTimeInterface;
use Illuminate\Support\Facades\DB;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * App\Models\BaseModel
 *
 * @method static Builder|BaseModel forceIndex(string $index)
 * @method static Builder|BaseModel newModelQuery()
 * @method static Builder|BaseModel newQuery()
 * @method static Builder|BaseModel query()
 *
 * @mixin Eloquent
 */
class BaseModel extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * 强制索引
     *
     * @param Builder $query
     * @param string $index
     * @return Builder
     */
    public function scopeForceIndex(Builder $query, string $index): Builder
    {
        $table = $this->getTable();

        return $query->from(DB::raw("`$table` FORCE INDEX(`$index`)"));
    }

    /**
     * 静态方法调用
     *
     * @return static
     */
    public static function instance(): static
    {
        return app(static::class);
    }

    /**
     * 主键检索
     *
     * @param int $primaryKey
     * @return Model
     */
    public static function findOne(int $primaryKey): Model
    {
        return self::instance()->find($primaryKey);
    }

    /**
     * 查询一条数据
     *
     * @param array $where
     * @param array $select
     * @param string $sortField
     * @param string $sortSeq
     * @return Model
     */
    public static function getOne(array $where = [], array $select = ['*'], string $sortField = 'id', string $sortSeq = 'asc'): Model
    {
        return self::instance()->select($select)->where($where)->orderBy($sortField, $sortSeq)->first();
    }

    /**
     * 指定条件检索单条数据
     *
     * @param string $field
     * @param string $value
     * @return Model
     */
    public static function findByField(string $field, string $value): Model
    {
        return self::instance()->where($field, $value)->first();
    }

    /**
     * 判断记录是否存在
     *
     * @param array $where
     * @return bool
     */
    public static function isExists(array $where): bool
    {
        return self::instance()->where($where)->exists();
    }

    /**
     * 获取某一列的值
     *
     * @param array $where
     * @param string $column
     * @param string|null $key
     * @return array
     */
    public static function getColumns(array $where, string $column, ?string $key = null): array
    {
        return self::instance()->where($where)->pluck($column, $key)->all();
    }

    /**
     * 检索单列值
     *
     * @param array $where
     * @param string $column
     * @return string|null
     */
    public static function getValue(array $where, string $column): string|null
    {
        return self::instance()->where($where)->value($column);
    }

    /**
     * 根据ID更新单条数据
     *
     * @param int $primaryKey
     * @param array $updateData
     * @return int
     */
    public static function updateById(int $primaryKey, array $updateData = []): int
    {
        return self::instance()->where('id', $primaryKey)->update($updateData);
    }

    /**
     * 查询数据集合
     *
     * @param array $where
     * @param array $select
     * @param string $sortField
     * @param string $sortSeq
     * @param string $groupBy
     * @return Collection
     */
    public static function getList(array $where = [], array $select = ['*'], string $sortField = 'id', string $sortSeq = 'asc', string $groupBy = ''): Collection
    {
        $query = self::instance()->select($select)->where($where)->orderBy($sortField, $sortSeq);
        if ($groupBy) {
            $query->groupBy($groupBy);
        }

        return $query->get();
        // return self::instance()->select($select)->where($where)->orderBy($sortField, $sortSeq)->get();
    }

    /**
     * 查询分页数据集合
     *
     * @param array $where
     * @param array $select
     * @param int $pageSize
     * @param string $sortField
     * @param string $sortSeq
     * @param string $groupBy
     * @return LengthAwarePaginator
     */
    public static function getPageList(array $where = [], array $select = ['*'], int $pageSize = 15, string $sortField = 'id', string $sortSeq = 'asc', string $groupBy = ''): LengthAwarePaginator
    {
        $query = self::instance()->select($select)->where($where)->orderBy($sortField, $sortSeq);
        if ($groupBy) {
            $query->groupBy($groupBy);
        }

        return $query->paginate($pageSize);
    }

    /**
     * 批量添加
     *
     * @param string $tableName 数据表名
     * @param array $info 插入的数据
     * @return bool
     *
     * @author kert
     */
    public function batchInsert(string $tableName, array $info): bool
    {
        $timeArray = [
            'create_at' => time(),
            'update_at' => time(),
        ];
        array_walk($info, function (&$value, $key, $timeArray) {
            $value = array_merge($value, $timeArray);
        }, $timeArray);

        return DB::table($tableName)->insert($info);
    }

    /**
     * 批量更新
     *
     * @param string $tableName 数据表名
     * @param array $info 更新的数据
     * @return int|string 受影响的行数
     *
     * @author kert
     */
    public function batchUpdate(string $tableName, array $info): int|string
    {
        try {
            if (!empty($info)) {
                $firstRow = current($info);
                $updateColumn = array_keys($firstRow);
                $referenceColumn = isset($firstRow['id']) ? 'id' : current($updateColumn);
                unset($updateColumn[0]);
                $updateSql = 'UPDATE ' . $tableName . ' SET ';
                $sets = [];
                $bindings = [];
                foreach ($updateColumn as $uColumn) {
                    $setSql = '`' . $uColumn . '` = CASE ';
                    foreach ($info as $data) {
                        $setSql .= 'WHEN `' . $referenceColumn . '` = ? THEN ? ';
                        $bindings[] = $data[$referenceColumn];
                        $bindings[] = $data[$uColumn];
                    }
                    $setSql .= 'ELSE `' . $uColumn . '` END ';
                    $sets[] = $setSql;
                }
                $updateSql .= implode(', ', $sets);
                $whereIn = collect($info)->pluck($referenceColumn)->values()->all();
                $bindings = array_merge($bindings, $whereIn);
                $whereIn = rtrim(str_repeat('?,', count($whereIn)), ',');
                $updateSql = rtrim($updateSql, ', ') . ' WHERE `' . $referenceColumn . '` IN (' . $whereIn . ')';

                return DB::update($updateSql, $bindings);
            }

            return 0;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
