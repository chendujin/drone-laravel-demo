<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Result;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Constants\Http\Status;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class InterceptHeaderMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response|RedirectResponse) $next
     * @return JsonResponse|mixed|RedirectResponse|Response
     */
    public function handle(Request $request, Closure $next): mixed
    {
        // 检查请求头中的某个值
        if (!$request->hasHeader('External-Merchant-Id')) {
            return Result::error(ErrorCode::EXTERNAL_MERCHANT_NOT_EXIST, httpStatus: Status::CODE_401);
        }
        $request->offsetSet('external_merchant_id', $request->header('External-Merchant-Id'));
        // 继续处理请求
        return $next($request);
    }
}
