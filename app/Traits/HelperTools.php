<?php

/**
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│_ -│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{ [│} ]│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │         Space         │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 */

namespace App\Traits;

use Illuminate\Support\Str;

trait HelperTools
{
    // 枚举字段转换为前端的格式
    public function enumFormat(array $data): array
    {
        return collect($data)->map(fn ($label, $value) => ['value' => $value, 'label' => $label])->values()->toArray();
    }

    /**
     * 读取CSV文件并输出数组格式数据
     *
     * @param string $csvFile
     * @return array
     */
    public function readCsv(string $csvFile = ''): array
    {
        // 数组键值
        $row = 0;
        // Excel数组
        $excelData = [];
        // 打开文件
        $file = fopen($csvFile, 'r');
        // 从文件指针中读入一行并解析 CSV 字段
        // fgetcsv 解析读入的行并找出 CSV 格式的字段然后返回一个包含这些字段的数组。
        while ($data = fgetcsv($file)) {
            // 统计一行数据有多少列
            $columnSize = count($data);
            for ($i = 0; $i < $columnSize; $i++) {
                // 转换字符的编码 && 赋值Excel源数据
                $excelData[$row][$i] = mb_convert_encoding(
                    $data[$i],
                    'UTF-8',
                    'gbk'
                );
            }
            $row++;
        }
        // 关闭一个已打开的文件指针
        fclose($file);
        return $excelData;
    }

    /**
     * 生成CSV文件
     *
     * @param array $data 数据
     * @param array $header_data 首行数据
     * @param string $file_name 文件名称
     * @return string
     */
    public function createCsv(array $data = [], array $header_data = [], string $file_name = ''): string
    {
        $fp = fopen(storage_path('app/public/' . $file_name), 'a');
        if (!empty($header_data)) {
            foreach ($header_data as $key => $value) {
                $header_data[$key] = iconv('utf-8', 'gbk', $value);
            }
            fputcsv($fp, $header_data);
        }
        $num = 0;
        // 每隔$limit行，刷新一下输出buffer，不要太大，也不要太小
        $limit = 100000;
        // 逐行取出数据，不浪费内存
        $count = count($data);
        if ($count > 0) {
            for ($i = 0; $i < $count; $i++) {
                $num++;
                // 刷新一下输出buffer，防止由于数据过多造成问题
                if ($limit == $num) {
                    ob_flush();
                    flush();
                    $num = 0;
                }
                $row = $data[$i];
                foreach ($row as $key => $value) {
                    $row[$key] = iconv('utf-8', 'gbk', $value);
                }
                fputcsv($fp, $row);
            }
        }
        fclose($fp);
        return storage_path('app/public/' . $file_name);
    }

    /**
     * 创建订单号(40位)
     *
     * @param string $prefix
     * @return string
     */
    public function createOrderNo(string $prefix = ''): string
    {
        $orderNo = Str::uuid()->toString();

        return $prefix . date('YmdHis') . substr((string) time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(1000, 9999)) . substr($orderNo, -12);
    }

    /**
     * 根据手机号码生成随机头像
     *
     * @param string $phone
     * @return string
     */
    public function makeAvatar(string $phone): string
    {
        $md5_email = md5($phone);
        return "https://api.multiavatar.com/{$md5_email}.png";
    }

    /**
     * 生成签名，防止请求参数篡改
     *
     * @param array $params
     * @param string $merchantSecret
     * @return string
     */
    public function makeSign(array $params, string $merchantSecret = ''): string
    {
        unset($params['sign']);
        $params['api_key'] = $merchantSecret; // 拼接api加密密钥
        ksort($params); // key升序
        $string_temp = http_build_query($params);
        return md5($string_temp);
    }

    /**
     * 生成随机字符串
     *
     * @param int $length 生成长度
     * @param int $type 生成类型：0-小写字母+数字，1-小写字母，2-大写字母，3-数字，4-小写+大写字母，5-小写+大写+数字
     * @return string 返回结果
     */
    public function getRandomStr(int $length = 8, int $type = 0): string
    {
        $a = 'abcdefghijklmnopqrstuvwxyz';
        $A = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $n = '0123456789';

        $chars = match ($type) {
            1 => $a,
            2 => $A,
            3 => $n,
            4 => $a . $A,
            5 => $a . $A . $n,
            default => $a . $n,
        };

        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $str;
    }

    /**
     * 字符串截取，支持中文和其他编码
     *
     * @param string $str 需要转换的字符串
     * @param int $start 开始位置
     * @param int|null $length 截取长度
     * @param string $encoding 编码格式
     * @param string $suffix 截断显示字符
     * @return false|string 返回结果
     */
    public function mbSubstr(string $str, int $start = 0, ?int $length = null, string $encoding = 'utf-8', string $suffix = '...'): bool|string
    {
        if (function_exists('mb_substr')) {
            $slice = mb_substr($str, $start, $length, $encoding);
        } elseif (function_exists('iconv_substr')) {
            $slice = iconv_substr($str, $start, $length, $encoding);
            if ($slice === false) {
                $slice = '';
            }
        } else {
            $re['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
            $re['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
            $re['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
            $re['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
            preg_match_all($re[$encoding], $str, $match);
            $slice = implode('', array_slice($match[0], $start, $length));
        }
        return $suffix ? $slice . $suffix : $slice;
    }
}
