<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use App\Channels\EnterpriseFeiShuBotChannel;

class SendExceptionMessageNotification extends Notification
{
    // use Queueable;

    public function via(): array
    {
        return [
            EnterpriseFeiShuBotChannel::class,
        ];
    }

    /**
     * @param mixed $notifiable
     * @return array
     */
    public function toBot(mixed $notifiable): array
    {
        return [
            'url' => config('tangerine.bot_url'),
            'msg_type' => $notifiable->routes['msg_type'],
            'type' => $notifiable->routes['type'],
            'content' => $notifiable->routes['content'],
        ];
    }
}
