<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Container\BindingResolutionException;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     *
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        /**
         * 添加请求traceId,方便日志链路追踪
         */
        $logger = $this->app->make('log');
        $traceId = md5(time() . mt_rand(1, 1000000));
        request()->offsetSet('traceId', $traceId);
        $logger->pushProcessor(function ($record) use ($traceId) {
            $record['context']['traceId'] = $traceId;
            return $record;
        });
    }
}
