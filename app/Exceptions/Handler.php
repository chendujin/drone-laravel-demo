<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\Response;
use App\Traits\ApiResponseTrait;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\ValidationException;
use App\Notifications\SendExceptionMessageNotification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponseTrait;

    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        NotFoundHttpException::class,
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        /**
         * 对于应用程序抛出了意料之外的异常,我们应该在生产环境中使用飞书机器人通知到开发者
         */
        $this->reportable(function (Throwable $e) {
            if (App::isProduction() && !$e instanceof BusinessException) {
                Notification::route('msg_type', 'interactive')
                    ->route('type', 'card')
                    ->route('content', $this->parsExceptionContent($e))
                    ->notify(new SendExceptionMessageNotification());
            }
        });
    }

    /**
     * 将异常转换为自定义异常抛出
     *
     * @param $request
     * @param Throwable $e
     * @return JsonResponse|Response|\Symfony\Component\HttpFoundation\Response
     *
     * @throws BusinessException
     * @throws Throwable
     */
    public function render($request, Throwable $e): Response|JsonResponse|\Symfony\Component\HttpFoundation\Response
    {
        // 请求类型错误异常抛出
        if ($e instanceof MethodNotAllowedHttpException) {
            $this->throwBusinessException(ErrorCode::CLIENT_METHOD_HTTP_TYPE_ERROR);
        }
        // 请求资源(路由)不存在异常抛出
        if ($e instanceof NotFoundHttpException) {
            // return Result::error(ErrorCode::CLIENT_NOT_FOUND_ERROR, ErrorCode::getMessage(ErrorCode::CLIENT_NOT_FOUND_ERROR));
            $this->throwBusinessException(ErrorCode::CLIENT_NOT_FOUND_ERROR);
        }
        // 参数校验错误异常抛出
        if ($e instanceof ValidationException) {
            $this->throwBusinessException(ErrorCode::CLIENT_PARAMETER_ERROR, str_replace(['。'], '', $e->getMessage()));
        }
        // 未授权异常抛出
        if ($e instanceof AuthenticationException) {
            $this->throwBusinessException(ErrorCode::CLIENT_HTTP_UNAUTHORIZED);
        }
        // 受中间件保护路由异常抛出
        if ($e instanceof RouteNotFoundException) {
            $this->throwBusinessException(ErrorCode::CLIENT_HTTP_UNAUTHORIZED);
        }
        // jwt授权验证异常抛出
        if ($e instanceof UnauthorizedHttpException) {
            // 拦截token异常抛出的错误(token已失效或者token不存在)
            $this->throwBusinessException(ErrorCode::ADMIN_UN_LOGIN, message: '无效的访问令牌');
        }
        // 模型找不到异常
        if ($e instanceof ModelNotFoundException) {
            $this->throwBusinessException(message: 'Entry for ' . str_replace('App\\', '', $e->getModel()) . ' not found');
        }
        // Http异常抛出
        if ($e instanceof HttpException) {
            $this->throwBusinessException(message: $e->getStatusCode() . $e->getMessage());
        }

        return parent::render($request, $e);
    }

    /**
     * @param Throwable $e
     * @return array
     */
    private function parsExceptionContent(Throwable $e): array
    {
        return [
            'header' => [
                'title' => [
                    'tag' => 'plain_text',
                    'content' => '⚠️应用程序出现了未知的异常⚠️',
                ],
            ],
            'config' => [
                'enable_forward' => true,
                'update_multi' => true,
            ],
            'elements' => [
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => '<at id=all></at>',
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**项目名称：**\n %s", config('app.name')),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**运行环境：**\n %s", config('app.env')),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**异常类：**\n %s", get_class($e)),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**异常消息：**\n %s", $e->getMessage()),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**错误码：**\n %s", $e->getCode()),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**异常文件：**\n %s【%s】", $e->getFile(), $e->getLine()),
                    ],
                ],
                [
                    'tag' => 'div',
                    'text' => [
                        'tag' => 'lark_md',
                        'content' => \sprintf("**Trace Info：**\n %s", $e->getTraceAsString()),
                    ],
                ],
            ],
        ];
    }
}
