<?php

namespace App\Channels;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use Illuminate\Notifications\Notification;

class EnterpriseFeiShuBotChannel
{
    /**
     * @param mixed $notifiable
     * @param Notification $notification
     * @return void
     */
    public function send(mixed $notifiable, Notification $notification): void
    {
        $data = $notification->toBot($notifiable);
        try {
            Http::connectTimeout(30)
                ->timeout(30)
                ->retry(2)
                ->post($data['url'], [
                    'msg_type' => $data['msg_type'],
                    $data['type'] => $data['content'],
                ])
                ->throw();
        } catch (Exception $exception) {
            Log::error('飞书机器人发送请求错误：' . $exception->getMessage());
        }
    }
}
