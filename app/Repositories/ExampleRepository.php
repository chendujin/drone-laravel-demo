<?php

namespace App\Repositories;

use App\Models\Admin\Admin;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ExampleRepository
{
    /**
     * @param array $queries
     * @return LengthAwarePaginator
     */
    public static function getList(array $queries): LengthAwarePaginator
    {
        $selects = ['id', 'name', 'created_at'];
        $query = Admin::query()
            ->select($selects);

        if (!empty($queries['name'])) {
            $query->where('name', 'LIKE', "%{$queries['name']}%");
        }

        return $query->orderByDesc('id')->paginate($queries['page_size'] ?? 15);
    }
}
