<?php

/**
 * 错误码常量类
 */

namespace App\Constants\Http;

class ErrorCode
{
    const SUCCESS = 0;

    const FAIL = 100001;

    const ADMIN_UN_LOGIN = 9999;

    /* ----------------------------------------1XX--------------------------------------------------- */
    const OLD_PASSWORD_WRONG = 100004;

    const LOGIN_TRY_ERROR = 100005;

    const GROUP_CREATE_FAILED = 100006;

    const ROLE_NOT_EXIST = 100007;

    const GROUP_UPDATE_FAILED = 100008;

    const ADMIN_NOT_EXIST = 100009;

    const MERCHANT_NOT_EXIST = 100010;

    const MERCHANT_ADMIN_IS_EXIST = 100011;

    const EXTERNAL_MERCHANT_NOT_EXIST = 100012;

    /* ----------------------------------------2XX--------------------------------------------------- */
    // 200表示服务器成功地接受了客户端请求
    const ADMIN_LOGIN_ERROR = 200002;

    const NO_PERMISSION = 200003;

    const ADMIN_ACCOUNT_DISABLED = 200004;

    const ADMIN_ACCOUNT_NO_ASSIGN_ROLE = 200005;

    const ADMIN_ACCOUNT_NO_ASSIGN_PERMISSION = 200006;

    const ADMIN_CREATE_FAILED = 200007;

    const ADMIN_UPDATE_FAILED = 200008;

    const ADMIN_DELETE_FAILED = 200009;

    const ADMIN_SET_STATUS_FAILED = 200010;

    const ADMIN_SWITCH_ONLINE_FAILED = 200011;

    const MERCHANT_CREATE_FAILED = 200101;

    const MERCHANT_UPDATE_FAILED = 200102;

    const MERCHANT_DELETE_FAILED = 200103;

    const MERCHANT_SET_STATUS_FAILED = 200104;

    const USER_SERVICE_LOGIN_SUCCESS = 200200;

    const USER_SERVICE_LOGIN_ERROR = 200201;

    const USER_SERVICE_LOGOUT_SUCCESS = 200202;

    const USER_SERVICE_LOGOUT_ERROR = 200203;

    const HTTP_ACTION_FREQUENT_ERROR = 200302;

    const ADMIN_ACCOUNT_INFO_ERROR = 200401;

    const ADMIN_ACCOUNT_NOT_EXIT = 200402;

    const ADMIN_ACCOUNT_LOGIN_ERROR = 200403;

    const SEARCH_INTERVAL_TIME_LIMIT = 200501;

    const RISK_RULES_SUBMIT_FAILED = 200601;

    const MERCHANT_FUND_IS_NOT_ENOUGH_FAILED = 200701;

    const MERCHANT_FUND_IS_MAX_ON_CREDIT_FAILED = 200702;

    const MERCHANT_ORDERS_DISTRIBUTES_FAILED = 200801;

    const MERCHANT_ORDERS_TRANSFER_FAILED = 200802;

    const MERCHANT_ORDERS_NOT_EXIT = 200803;

    const MERCHANT_ORDERS_REVIEW_SUBMIT_FAILED = 200804;

    const MERCHANT_ORDERS_ORDER_STATUS_NOT_PENDING_FAILED = 200805;

    const MERCHANT_ORDERS_BILLS_REDUCE_SUBMIT_FAILED = 200806;

    const MERCHANT_ORDERS_BILLS_NOT_EXIT = 200807;

    const MERCHANT_ORDERS_UPDATE_ORDER_STATISTICS_FAILED = 200808;

    const MERCHANT_ORDERS_BILLS_REDUCE_DISABLED = 200809;

    const MERCHANT_CUSTOMER_NOT_EXIT = 200810;

    /* ----------------------------------------4XX--------------------------------------------------- */
    // 400 - 错误的请求
    const CLIENT_NOT_FOUND_HTTP_ERROR = 400001;

    const CLIENT_PARAMETER_ERROR = 400200;

    const CLIENT_CREATED_ERROR = 400201;

    const CLIENT_DELETED_ERROR = 400202;

    // 401 - 访问被拒绝
    const CLIENT_HTTP_UNAUTHORIZED = 400101;

    const CLIENT_HTTP_UNAUTHORIZED_EXPIRED = 400102;

    const CLIENT_HTTP_UNAUTHORIZED_BLACKLISTED = 400103;

    // 403 - 禁止访问
    // 404 - 没有找到文件或目录
    const CLIENT_NOT_FOUND_ERROR = 404001;

    // 405 - 用来访问本页面的 HTTP 谓词不被允许（方法不被允许）
    const CLIENT_METHOD_HTTP_TYPE_ERROR = 405001;

    // 文件上传相关
    const UPLOAD_FILE_ILLEGAL_ERROR = 406001;

    const UPLOAD_FILE_TOO_LARGE_ERROR = 406002;

    const UPLOAD_FILE_EXIST_ERROR = 406003;

    const UPLOAD_FILE_FAILED_ERROR = 406004;

    /* -----------------------------------------5XX-------------------------------------------------- */
    // 服务端操作错误码：500 ~ 599 开头，后拼接 3 位
    // 500 - 内部服务器错误
    const SERVER_ERROR = 500001;

    const DATABASE_ERROR = 500002;

    const SERVER_UNAVAILABLE = 503001;

    const MERCHANT_ORDERS_IS_EXISTS = 600001;

    const MERCHANT_ORDERS_SYNC_PUSH_FAILED = 600002;

    const MERCHANT_OVERDUE_ORDERS_SYNC_PUSH_FAILED = 600003;

    const MERCHANT_BILLS_SYNC_PUSH_FAILED = 600004;

    const MERCHANT_BILLS_ORDERS_NOT_EXISTS = 600005;

    const MERCHANT_BILLS_ORDERS_NOT_REPAYMENT = 600006;

    public static array $message = [
        self::SUCCESS => '操作成功',
        self::FAIL => '操作失败',

        self::GROUP_CREATE_FAILED => '创建角色异常，请稍后重试',
        self::GROUP_UPDATE_FAILED => '更新角色异常，请稍后重试',
        self::ROLE_NOT_EXIST => '获取角色信息失败',
        self::ADMIN_NOT_EXIST => '用户不存在！',
        self::MERCHANT_NOT_EXIST => '商户不存在！',
        self::MERCHANT_ADMIN_IS_EXIST => '登录账号已存在',
        self::EXTERNAL_MERCHANT_NOT_EXIST => '商户标识异常，请联系商务对接',
        self::OLD_PASSWORD_WRONG => '原密码错误',
        self::LOGIN_TRY_ERROR => '输错密码次数太多，请一小时后再试！',
        self::NO_PERMISSION => '无权访问，请联系管理员！',
        self::ADMIN_ACCOUNT_DISABLED => '您的账户被禁用，请联系管理员！',
        self::ADMIN_ACCOUNT_NO_ASSIGN_ROLE => '您的账户暂无角色分组，登录失败',
        self::ADMIN_ACCOUNT_NO_ASSIGN_PERMISSION => '您的账户暂无权限分配，登录失败',
        self::ADMIN_CREATE_FAILED => '创建账户异常，请稍后重试',
        self::ADMIN_UPDATE_FAILED => '更新账户异常，请稍后重试',
        self::ADMIN_DELETE_FAILED => '删除账户异常，请稍后重试',
        self::ADMIN_SET_STATUS_FAILED => '设置账户状态异常，请稍后重试',
        self::ADMIN_SWITCH_ONLINE_FAILED => '切换管理员上下线状态异常，请稍后重试',

        self::MERCHANT_CREATE_FAILED => '创建商户异常，请稍后重试',
        self::MERCHANT_UPDATE_FAILED => '更新商户异常，请稍后重试',
        self::MERCHANT_DELETE_FAILED => '删除商户异常，请稍后重试',
        self::MERCHANT_SET_STATUS_FAILED => '设置商户状态异常，请稍后重试',

        self::ADMIN_UN_LOGIN => '未登录,请先登录',
        self::ADMIN_LOGIN_ERROR => '用户名或密码错误！',

        self::HTTP_ACTION_FREQUENT_ERROR => '操作频繁',
        self::USER_SERVICE_LOGIN_SUCCESS => '登录成功',
        self::USER_SERVICE_LOGIN_ERROR => '登录失败',
        self::USER_SERVICE_LOGOUT_SUCCESS => '退出登录成功',
        self::USER_SERVICE_LOGOUT_ERROR => '退出登录失败',
        self::ADMIN_ACCOUNT_INFO_ERROR => '获取管理员信息失败',
        self::ADMIN_ACCOUNT_NOT_EXIT => '账号不存在,登录失败',
        self::ADMIN_ACCOUNT_LOGIN_ERROR => '账号不存在或密码错误！',

        self::SEARCH_INTERVAL_TIME_LIMIT => '查询时间不能超过180天',

        self::RISK_RULES_SUBMIT_FAILED => '提交规则配置失败',

        self::MERCHANT_FUND_IS_NOT_ENOUGH_FAILED => '商户资金不足，扣款失败',
        self::MERCHANT_FUND_IS_MAX_ON_CREDIT_FAILED => '商户资金已达最大欠款额度，扣款失败',

        self::MERCHANT_ORDERS_DISTRIBUTES_FAILED => '任务分派异常，请稍后重试',
        self::MERCHANT_ORDERS_TRANSFER_FAILED => '任务流转异常，请稍后重试',
        self::MERCHANT_ORDERS_NOT_EXIT => '商家订单不存在，请稍后重试',
        self::MERCHANT_ORDERS_REVIEW_SUBMIT_FAILED => '商家订单审核异常，请稍后重试',
        self::MERCHANT_ORDERS_ORDER_STATUS_NOT_PENDING_FAILED => '商家订单非待审核状态，审核失败',
        self::MERCHANT_ORDERS_BILLS_REDUCE_SUBMIT_FAILED => '提交账单减免金额异常，请稍后重试',
        self::MERCHANT_ORDERS_BILLS_NOT_EXIT => '客户账单不存在，请稍后重试',
        self::MERCHANT_ORDERS_UPDATE_ORDER_STATISTICS_FAILED => '更新订单统计异常，请稍后重试',
        self::MERCHANT_ORDERS_BILLS_REDUCE_DISABLED => '暂时无法减免账单金额',
        self::MERCHANT_CUSTOMER_NOT_EXIT => '客户信息不存在，请稍后重试',

        self::CLIENT_NOT_FOUND_HTTP_ERROR => '请求失败',
        self::CLIENT_HTTP_UNAUTHORIZED => '授权失败,请先登录',
        self::CLIENT_PARAMETER_ERROR => '请求参数错误',
        self::CLIENT_CREATED_ERROR => '数据已存在',
        self::CLIENT_DELETED_ERROR => '数据不存在',
        self::CLIENT_HTTP_UNAUTHORIZED_EXPIRED => '账号信息已过期，请重新登录',
        self::CLIENT_HTTP_UNAUTHORIZED_BLACKLISTED => '账号在其他设备登录，请重新登录',
        self::CLIENT_NOT_FOUND_ERROR => '请求资源不存在',
        self::CLIENT_METHOD_HTTP_TYPE_ERROR => 'HTTP请求类型错误',
        self::UPLOAD_FILE_ILLEGAL_ERROR => '非法文件上传,支持文件后缀格式：jpg,jpeg,gif,png',
        self::UPLOAD_FILE_TOO_LARGE_ERROR => '上传文件过大',
        self::UPLOAD_FILE_EXIST_ERROR => '文件已经存在',
        self::UPLOAD_FILE_FAILED_ERROR => '文件上传失败',

        self::SERVER_ERROR => '服务器错误',
        self::DATABASE_ERROR => '数据库正忙请稍后再试',
        self::SERVER_UNAVAILABLE => '服务器正在维护中',

        self::MERCHANT_ORDERS_IS_EXISTS => '商家订单已存在',
        self::MERCHANT_ORDERS_SYNC_PUSH_FAILED => '同步商家待审核订单失败',
        self::MERCHANT_OVERDUE_ORDERS_SYNC_PUSH_FAILED => '同步商家逾期订单失败',
        self::MERCHANT_BILLS_SYNC_PUSH_FAILED => '同步商家账单状态失败',
        self::MERCHANT_BILLS_ORDERS_NOT_EXISTS => '商家账单信息不存在',
        self::MERCHANT_BILLS_ORDERS_NOT_REPAYMENT => '非已还款状态账单状态',
    ];

    public static function getMessage(int $code): string
    {
        return self::$message[$code] ?? self::$message[self::SERVER_ERROR];
    }
}
