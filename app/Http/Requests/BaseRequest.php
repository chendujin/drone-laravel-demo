<?php

namespace App\Http\Requests;

use App\Traits\ApiResponseTrait;
use App\Constants\Http\ErrorCode;
use App\Exceptions\BusinessException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BaseRequest extends FormRequest
{
    use ApiResponseTrait;

    /**
     * 默认允许访问, 权限控制中间件已经进行过滤了！
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $actionMethod = $this->route()->getActionMethod() . 'Rules';

        if (!method_exists($this, $actionMethod)) {
            return [];
        }

        return $this->$actionMethod();
    }

    public function messages(): array
    {
        $actionMethod = $this->route()->getActionMethod() . 'Messages';

        if (!method_exists($this, $actionMethod)) {
            return [];
        }

        return $this->$actionMethod();
    }

    public function attributes(): array
    {
        $actionMethod = $this->route()->getActionMethod() . 'Attributes';

        if (!method_exists($this, $actionMethod)) {
            return [];
        }

        return $this->$actionMethod();
    }

    /**
     * @param Validator $validator
     *
     * @throws BusinessException
     */
    protected function failedValidation(Validator $validator): void
    {
        $errorMsg = $validator->errors()->first();
        // 将句号替换成空
        $info = str_replace(['。'], '', $errorMsg);
        $this->throwBusinessException(ErrorCode::CLIENT_PARAMETER_ERROR, $info);
    }
}
