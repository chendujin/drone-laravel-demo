<?php

namespace App\Exceptions;

use Exception;
use App\Helpers\Result;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;

/**
 * 自定义业务处理异常类
 */
class BusinessException extends Exception
{
    public function __construct(int $code = ErrorCode::FAIL, string $message = '', ?Exception $previous = null)
    {
        $message = $message ?: ErrorCode::getMessage($code);
        parent::__construct($message, $code, $previous);
    }

    public function render(): JsonResponse
    {
        return Result::error($this->getCode(), $this->getMessage());
    }
}
