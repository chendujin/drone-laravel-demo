<?php

namespace App\Traits;

use Throwable;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Exceptions\ApiClientRequestException;

trait ApiClientRequestTrait
{
    /**
     * GET
     *
     * @param string $url
     * @param array $queries
     * @param array $headers
     * @param array $interfaceDetail
     * @return object|null
     */
    public function httpGet(string $url, array $queries = [], array $headers = [], array $interfaceDetail = []): ?object
    {
        return $this->send($url, 'GET', 'query', $queries, $headers, $interfaceDetail);
    }

    /**
     * post application/x-www-form-urlencoded
     *
     * @param string $url
     * @param array $data
     * @param array $headers
     * @param array $interfaceDetail
     * @return object|null
     */
    public function httpPost(string $url, array $data, array $headers = [], array $interfaceDetail = []): ?object
    {
        return $this->send($url, 'POST', 'form_params', $data, $headers, $interfaceDetail);
    }

    /**
     * post application/json => json
     *
     * @param string $url
     * @param array $data
     * @param array $headers
     * @param array $interfaceDetail
     * @return object|null
     */
    public function httpPostJson(string $url, array $data, array $headers = [], array $interfaceDetail = []): ?object
    {
        return $this->send($url, 'POST', 'json', $data, $headers, $interfaceDetail);
    }

    /**
     * @param string $url 请求url
     * @param string $method 请求的方式
     * @param string $contentType 数据类型
     * @param array $request 请求的数据
     * @param array $headers 请求头
     * @param array $interfaceDetail 额外数据
     * @return object|null
     */
    protected function send(string $url, string $method, string $contentType, array $request, array $headers = [], array $interfaceDetail = []): ?object
    {
        $interfaceDetail = array_merge(
            $interfaceDetail,
            ['full_url' => $url]
        );
        $options = array_merge(
            $this->getDefaultOptions(),
            ['headers' => $headers, $contentType => $request]
        );
        $client = new Client($options);
        try {
            Log::info(__CLASS__ . ' Request:', compact('url', 'method', 'contentType', 'request', 'headers'));

            $response = $client->request($method, $url);
            $result = json_decode($response->getBody()->getContents());

            Log::info(__CLASS__ . ' Response:', (array) $result);
        } catch (Throwable $exception) {
            Log::error('发送的请求出现了异常:' . $exception->getMessage());
            /**
             * 将异常上报到飞书机器人
             */
            report(new ApiClientRequestException(
                message: $exception->getMessage(),
                method: $method,
                url: $url,
                body: $request,
                interfaceDetail: $interfaceDetail
            ));

            return null;
        } finally {
            // 流水记录
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getDefaultOptions(): array
    {
        return [
            'verify' => false,
            'timeout' => 30,
        ];
    }
}
