<?php

namespace App\Services;

use App\Traits\ApiResponseTrait;

class BaseService
{
    // 引入 API 统一返回消息
    use ApiResponseTrait;

    // protected static BaseService $instance;
    //
    // public static function getInstance(): BaseService|static
    // {
    //     if (static::$instance instanceof static) {
    //         return self::$instance;
    //     }
    //     static::$instance = new static();
    //     return self::$instance;
    // }
    //
    // protected function __construct()
    // {
    //
    // }
    //
    // protected function __clone()
    // {
    //
    // }
}
