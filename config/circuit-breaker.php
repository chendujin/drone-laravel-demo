<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Laravel 断路器配置文件
    | Laravel circuit breaker Configuration
    |--------------------------------------------------------------------------
    |
    | This package is a simple implementation of circuit breaker pattern for laravel.
    | It protects your application from failures of its service dependencies.
    |
    |
    */

    // 可以在此处指定要使用哪个缓存存储作为默认存储
    'store' => config('cache.default'),

    // 计算错误率的时间间隔长度（以秒为单位）
    'time_window' => 60,

    // 断开电路之前给定时间内遇到的错误数
    'error_threshold' => 10,

    // 断路器再次尝试查询资源之前的时间
    'error_timeout' => 300,

    // 电路处于半开状态时的超时时间
    'half_open_timeout' => 150,

    // 电路再次闭合的连续成功次数
    'success_threshold' => 1,

];
