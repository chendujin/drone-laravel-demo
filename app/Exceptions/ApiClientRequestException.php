<?php

namespace App\Exceptions;

use Exception;
use App\Constants\Http\ErrorCode;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendExceptionMessageNotification;

class ApiClientRequestException extends Exception
{
    public function __construct(
        int $code = ErrorCode::FAIL,
        string $message = '', ?Exception $previous = null,
        public string $method = '',
        public string $url = '',
        public array $body = [],
        public array $response = [],
        public array $interfaceDetail = []
    ) {
        $message = $message ?: ErrorCode::getMessage($code);
        parent::__construct($message, $code, $previous);
    }

    public function report(): void
    {
        if (!App::isLocal()) {
            Notification::route('msg_type', 'interactive')
                ->route('type', 'card')
                ->route('content', $this->parsExceptionContent())
                ->notify(new SendExceptionMessageNotification());
        }
    }

    private function parsExceptionContent(): array
    {
        $elements = [
            [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**运行环境：**\n %s", config('app.env')),
                ],
            ],
            [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**Message：**\n %s", str_replace(["\r\n", "\r", "\n", '`'], '', $this->getMessage())),
                ],
            ],
            [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**Url：**\n [%s]%s", $this->method, $this->url),
                ],
            ],
            [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**Body：**\n %s", json_encode($this->body, JSON_UNESCAPED_UNICODE)),
                ],
            ],
        ];

        if ($this->response) {
            $elements[] = [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**Response：**\n %s", json_encode($this->response, JSON_UNESCAPED_UNICODE)),
                ],
            ];
        }

        if ($this->interfaceDetail) {
            $elements[] = [
                'tag' => 'div',
                'text' => [
                    'tag' => 'lark_md',
                    'content' => \sprintf("**InterfaceDetail：**\n %s", json_encode($this->interfaceDetail, JSON_UNESCAPED_UNICODE)),
                ],
            ];
        }

        return [
            'header' => [
                'title' => [
                    'tag' => 'plain_text',
                    'content' => '⚠️进销存系统请求三方API服务接口出现了错误⚠️',
                ],
            ],
            'config' => [
                'enable_forward' => true,
                'update_multi' => true,
            ],
            'elements' => $elements,
        ];
    }
}
