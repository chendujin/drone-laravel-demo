<?php

namespace App\Services\Common;

use App\Services\BaseService;

class AesService extends BaseService
{
    /**
     * php>=7.1
     * AES-256-CBC 加密
     *
     * @param string $data
     * @param string $key
     * @param string $iv
     * @return string
     */
    public static function encryptCbc(string $data, string $key, string $iv): string
    {
        return base64_encode(openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));
    }

    /**
     * php>=7.1
     * AES-256-CBC 解密
     *
     * @param string $text
     * @param string $key
     * @param string $iv
     * @return string
     */
    public static function decryptCbc(string $text, string $key, string $iv): string
    {
        return openssl_decrypt(base64_decode($text), 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
    }
}
