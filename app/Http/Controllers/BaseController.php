<?php

namespace App\Http\Controllers;

use App\Traits\HelperTools;
use Illuminate\Http\Request;
use App\Traits\ApiResponseTrait;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use App\Traits\VerifyRequestInputTrait;

class BaseController extends Controller
{
    // API接口响应
    use ApiResponseTrait, HelperTools;

    // 验证表单参数输入请求
    use VerifyRequestInputTrait;

    /**
     * 模块名称
     *
     * @var string
     */
    protected string $module = '';

    /**
     * 分组名称
     * 二级控制器的文件夹名称
     *
     * @var string
     */
    protected string $group = '';

    /**
     * 控制器名称
     *
     * @var string
     */
    protected string $controller;

    /**
     * 方法名称
     *
     * @var string
     */
    protected string $action;

    /**
     * Request实例
     *
     * @var Request
     */
    protected Request $request;

    /**
     * 构造函数
     * BaseController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->__initialize();
    }

    /**
     * 自定义的初始化方法
     */
    public function __initialize(): void
    {
        if (!App::runningInConsole()) {
            $routeName = $this->request->route()->getActionName();
            [$controller, $action] = explode('@', $routeName);
            $controller = str_replace('Controller', '', class_basename($controller));
            $this->controller = strtolower($controller);
            $this->action = $action;
        }
    }
}
