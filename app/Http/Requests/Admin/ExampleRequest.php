<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\BaseRequest;

class ExampleRequest extends BaseRequest
{
    public function distributesRules(): array
    {
        return ['task_ids' => 'required|string', 'admin_id' => 'required|numeric'];
    }

    public function distributesMessages(): array
    {
        return [
            'task_ids.required' => '任务ID不能为空',
            'task_ids.string' => '任务ID必须为字符串',
            'admin_id.required' => '审核人员ID不能为空',
            'admin_id.numeric' => '审核人员ID必须为数值',
        ];
    }

    public function transfersRules(): array
    {
        return ['task_ids' => 'required|string', 'admin_id' => 'required|numeric'];
    }

    public function transfersMessages(): array
    {
        return [
            'task_ids.required' => '任务ID不能为空',
            'task_ids.string' => '任务ID必须为字符串',
            'admin_id.required' => '流转人员ID不能为空',
            'admin_id.numeric' => '流转人员ID必须为数值',
        ];
    }
}
