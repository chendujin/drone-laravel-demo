<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\BaseController;

class UploadsController extends BaseController
{
    public function upload(Request $request): JsonResponse
    {
        // 判断有文件上传且文件上传成功
        if ($request->isMethod('POST') && $request->hasFile('file') && $request->file('file')->isValid()) {
            // 获取上传文件
            $file = $request->file('file');
            // 获取文件后缀
            $ext = $file->getClientOriginalExtension();
            // 获取上传文件大小
            $size = $file->getSize();

            // 允许文件上传格式
            $allowExt = ['jpg', 'jpeg', 'gif', 'png'];
            // 允许文件上传大小
            $allowSize = 1024 * 1024;
            // 新文件名
            $newFileName = date('Ymd') . md5(time() . rand(100000, 999999)) . '.' . $ext;

            // 文件保存目录
            $saveFileDir = substr(md5($newFileName), 0, 2) . '/';
            // 文件保存路径(相对路径)
            $filePath = $saveFileDir . $newFileName;

            if (!in_array($ext, $allowExt)) {
                return $this->fail(ErrorCode::UPLOAD_FILE_ILLEGAL_ERROR);
            }

            if ($size > $allowSize) {
                return $this->fail(ErrorCode::UPLOAD_FILE_TOO_LARGE_ERROR);
            }

            if (Storage::disk('oss')->has($filePath)) {
                return $this->fail(ErrorCode::UPLOAD_FILE_EXIST_ERROR);
            }

            if ($file->storePubliclyAs($saveFileDir, $newFileName, ['disk' => 'oss'])) {
                $sitePath = config('site.oss_domain_path');
                $url = $sitePath . '/' . $filePath;
                // 文件上传成功后并返回文件路径
                return $this->success(['path' => $filePath, 'url' => $url], message: '文件上传成功');
            }

            return $this->fail(ErrorCode::UPLOAD_FILE_FAILED_ERROR);
        }

        return $this->fail(ErrorCode::UPLOAD_FILE_FAILED_ERROR);
    }
}
