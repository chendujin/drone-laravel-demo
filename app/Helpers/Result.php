<?php

namespace App\Helpers;

use App\Constants\Http\Status;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Result
{
    /**
     * 成功的JSON
     *
     * @param array $data
     * @param array $extend
     * @return JsonResponse
     */
    public static function success(array $data = [], array $extend = []): JsonResponse
    {
        return static::message('', true, $data, ErrorCode::SUCCESS, $extend);
    }

    /**
     * 失败JSON
     *
     * @param int $code
     * @param string $msg
     * @param int $httpStatus
     * @return JsonResponse
     */
    public static function error(int $code = ErrorCode::FAIL, string $msg = '', int $httpStatus = Status::CODE_200): JsonResponse
    {
        return static::message($msg ?: ErrorCode::getMessage($code), false, [], $code, [], $httpStatus);
    }

    /**
     * 返回json响应信息
     *
     * @param string $msg
     * @param bool $success
     * @param array $data
     * @param int $code
     * @param array $extend
     * @param int $httpStatus
     * @return JsonResponse
     */
    public static function message(string $msg, bool $success, array $data, int $code = ErrorCode::SUCCESS, array $extend = [], int $httpStatus = Status::CODE_200): JsonResponse
    {
        $rs = [
            'code' => $code,
            'msg' => $msg ?: '',
            'data' => $data,
            'success' => $success,
            'trace_id' => request()->input('traceId', ''),
        ];
        if ($extend) {
            foreach ($extend as $key => $value) {
                $rs[$key] = $value;
            }
        }
        Log::info('AppResponseJson：', $rs);
        $headers = ['content-type' => 'application/json'];

        return response()->json($rs, $httpStatus)->withHeaders($headers);
    }

    /**
     * 返回一个数组
     *
     * @param array $data
     * @return JsonResponse
     */
    public static function response(array $data): JsonResponse
    {
        return response()->json($data);
    }

    // 开启查询日志
    public static function enableSqlLog(): void
    {
        DB::connection()->enableQueryLog();
    }

    /**
     * 打印查询日志
     *
     * @param bool $simple
     * @return void
     */
    public static function printSqlLog(bool $simple = false): void
    {
        foreach (DB::getQueryLog() as $sql) {
            if (!$simple) {
                dump($sql);
            } else {
                dump($sql['query']);
            }
        }
        exit();
    }
}
