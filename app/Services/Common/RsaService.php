<?php

namespace App\Services\Common;

use OpenSSLAsymmetricKey;
use App\Services\BaseService;

class RsaService extends BaseService
{
    /**
     * 获取私钥
     *
     * @param string $privateKey
     * @return false|OpenSSLAsymmetricKey
     */
    private static function getPrivateKey(string $privateKey): false|OpenSSLAsymmetricKey
    {
        return openssl_pkey_get_private($privateKey);
    }

    /**
     * 获取公钥
     *
     * @param string $publicKey
     * @return false|OpenSSLAsymmetricKey
     */
    private static function getPublicKey(string $publicKey): false|OpenSSLAsymmetricKey
    {
        return openssl_pkey_get_public($publicKey);
    }

    /**
     * 私钥加密
     *
     * @param array $data
     * @param string $privateKey
     * @return string|null
     */
    public static function privateEncrypt(array $data = [], string $privateKey = ''): ?string
    {
        if (!is_array($data)) {
            return null;
        }
        return openssl_private_encrypt(json_encode($data, JSON_UNESCAPED_UNICODE), $encrypted, self::getPrivateKey($privateKey)) ? base64_encode($encrypted) : null;
    }

    /**
     * 公钥加密
     *
     * @param array $data
     * @param string $publicKey
     * @return string|null
     */
    public static function publicEncrypt(array $data = [], string $publicKey = ''): ?string
    {
        if (!is_array($data)) {
            return null;
        }
        return openssl_public_encrypt(json_encode($data, JSON_UNESCAPED_UNICODE), $encrypted, self::getPublicKey($publicKey)) ? base64_encode($encrypted) : null;
    }

    /**
     * 私钥解密
     *
     * @param string $encrypted
     * @param string $privateKey
     * @return string|null
     */
    public static function privateDecrypt(string $encrypted = '', string $privateKey = ''): ?string
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_private_decrypt(base64_decode($encrypted), $decrypted, self::getPrivateKey($privateKey))) ? $decrypted : null;
    }

    /**
     * 公钥解密
     *
     * @param string $encrypted
     * @param string $publicKey
     * @return string|null
     */
    public static function publicDecrypt(string $encrypted = '', string $publicKey = ''): ?string
    {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_public_decrypt(base64_decode($encrypted), $decrypted, self::getPublicKey($publicKey))) ? $decrypted : null;
    }
}
