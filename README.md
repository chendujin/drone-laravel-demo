<h1 align="center">Laravel9项目基础框架</h1>
<p align="center">
    <img src="https://img.shields.io/badge/Laravel-9.52-green" />
    <img src="https://img.shields.io/badge/PHP-8.0-green" />
    <img src="https://img.shields.io/badge/MySQL-8.0-green" />
    <img src="https://img.shields.io/badge/Composer-2.5.5-green" />
    <img src="https://img.shields.io/badge/Redis-5.0.3-green" />
</p>

## 项目概述

* 项目名称：Laravel9项目基础框架
* 项目代号：`laravel-basic`
* GitHub地址：https://github.com/chendujin/laravel-basic.git

## 目录结构
```
├── app                                                        # Laravel框架的应用核心代码目录
│   ├── Common                                                 # 共有类目录
│   ├── Console                                                # 终端控制目录，根据业务或模块创建对应文件夹集成
│   │   ├── Commands
│   │   │   │── Admin
│   │   │   │   └── CmdCli.php
│   │   └── Kernel.php
│   ├── Constants                                              # 枚举类层目录，譬如响应码表
│   │   └── ErrorCode.php
│   ├── Events                                                 # 事件目录
│   ├── Exceptions                                             # 异常目录
│   │   └── Handler.php
│   ├── Helpers                                                # 辅助类，比如数组(ArrayHelper)、字符串(StringHelper)辅助类
│   ├── Http                                                   # HTTP层目录
│   │   ├── Controllers                                        # 控制器层目录，根据业务或模块创建对应文件夹集成
│   │   │   ├── Admin
│   │   │   │   └── DemoController.php
│   │   │   ├── BaseController.php                             # 务必建立控制器基类
│   │   ├── Requests                                           # 请求句柄目录，根据业务或模块创建对应文件夹集成
│   │   │   ├── Admin
│   │   │   │   └── AdminRequest.php
│   │   │   ├── Api
│   │   │   ├── BaseRequest.php                                # 务必建立请求句柄基类
│   │   ├── Kernel.php
│   │   └── Middleware                                         # 中间件目录
│   ├── Jobs                                                   # 队列
│   │   └── Queue.php
│   ├── Lib                                                    # 项目稍微常改动的库文件
│   ├── Listeners                                              # 监听器目录
│   ├── Models                                                 # 模型层目录，根据业务或模块创建对应文件夹集成
│   │   ├── BaseModel.php                                      # 务必构建模型基类
│   │   └── User.php
│   ├── Notifications                                          # 发送通知目录
│   ├── Providers                                              # 服务提供注册目录
│   │   ├── AppServiceProvider.php
│   │   ├── AuthServiceProvider.php
│   │   ├── BroadcastServiceProvider.php
│   │   ├── EventServiceProvider.php
│   │   └── RouteServiceProvider.php
│   ├── Repositories                                           # 资源层目录，根据业务或模块创建对应文件夹集成
│   │   └── AdminRepository.php
│   ├── Services                                               # 业务层目录，根据业务或模块创建对应文件夹集成
│   │   ├── Admin
│   │   │   └── AdminService.php
│   │   ├── Api
│   │   ├── BaseService.php                                    # 务必构建业务基类
│   ├── Utils                                                  # 工具包类
│   └── Traits                                                 # Traits类
├── config                                                     # 配置文件目录
├── routes                                                     # 路由配置目录，根据业务或模块创建对应文件夹集成
├── docker                                                     # 镜像构建目录
│   ├── etc                                                    # 任务配置目录
│   ├── nginx                                                  # nginx配置目录
│   ├── prod                                                   # 生产环境配置目录
│   └── test                                                   # 测试环境配置目录
├── tests                                                      # 单元测试目录
└── bin                                                        # 项目依赖或自定义可执行程序目录
```

## 流程设计模式
每个人对于项目架构的认知不一，但在`Team`开发项目时，必须统一开发模式甚至是架构。从代码的稳定性、灵活性以及健壮性而言，极力推荐使用 Repository 模式，各执其职、各层之间抽离解耦。
大致流程简单表示为 `Controller` <=> `Service` <=>`Repository` <=> `Model | Cache | IOStream`。

### 流程示例图
![](https://image-albatross.ddzuwu.com/4a/2023091537695eda7948d38017c65e9f04ae9eb7.png)

## 系统要求

- Nginx 1.18+
- PHP 8.0+
- Composer
- Mysql 5.7+
- Redis 3.0+

## 运行环境
| 环境 | 接口域名                     | 后台域名                       |
| --- |--------------------------|----------------------------|
| 本地环境 | http://192.168.0.xxx/api | http://192.168.0.xxx/admin |
| 测试环境 | https://api-test.xxx.com | https://www-test.xxx.com   |
| 正式环境 | https://api.xxx.com      | https://www.xxx.com        |

后台管理员账号密码如下:

```
测试环境：
username: thomas
password: 123456
```

## 开发环境部署/安装

本项目代码使用 PHP 框架 [Laravel 9.52](https://learnku.com/docs/laravel/9.x) 开发。

### 基础安装

#### 1. 克隆源代码

克隆 `laravel-basic` 源代码到本地：
```shell
git clone git@github.com:chendujin/laravel-basic.git
```

#### 2. 安装扩展包依赖
```shell
composer install
```

#### 3. 生成配置文件

```shell
cp .env.example .env
```

你可以根据情况修改 `.env` 文件里的内容，如数据库连接、缓存、邮件设置等：

```
APP_URL=http://laravel-basic.test
...
DB_HOST=localhost
DB_DATABASE=laravel-basic
DB_USERNAME=root
DB_PASSWORD=123456

DOMAIN=laravel-basic.test
```
> 因 `.env` 不会被纳入版本控制器中，所以本地 `.env` 里添加变量时 必须 同步到 `.env.example` 中，以免影响其他项目参与者的工作。

#### 4. 生成秘钥

```shell
php artisan key:generate
```

#### 5. 生成`jwt`秘钥
```shell
php artisan jwt:secret
```

#### 6. 配置`hosts`文件
```shell
echo "192.168.0.***   laravel-basic.test" | sudo tee -a /etc/hosts
```

至此, 安装完成 ^_^。

## 编码风格统一工具
不同的团队有不同的代码标准规范，不同的开发者有不同的代码风格。
为了约束PHP代码风格，使用`laravel/pint`代码风格统一工具😁。

### 使用方法
每次编写完代码，提交代码之前在命令行中手动运行以下命令即可自动修复代码格式：
```
composer cs-fix
```
> 另提供静态代码分析命令，分析级别为level-6，可执行命令：composer analyse

## 开发规范约定
详细团队`开发规范/编码风格`请阅读：[团队项目PHP编码风格](https://sie49j3tw7c.feishu.cn/docx/XsMXdf5FzoJxAOxSeZlcwgvInsh)
#### 1.项目代码风格
> 代码风格必须严格遵循 `PSR-12` 规范。

#### 2.配置信息与环境变量
> 所有程序配置信息 必须通过 `config()` 来读取，所有的 `.env` 配置信息必须通过 `env()` 来读取，绝不在配置文件以外的范围使用 `env()`。

#### 3.路由定义
> 绝不在路由配置文件里书写`闭包路由`或者其他业务逻辑代码，因为一旦使用将无法使用 **_路由缓存_** 。 
> 路由器要保持干净整洁，绝不放置除路由配置以外的其他程序逻辑。

## Git flow常用分支

* Master: 用来放稳定、随时可上线的版本。只能从别的分支合并过来，不能直接提交到master
* Dev: 所有开发的基础分支，新feature分支从这出去，也会合并回这来
* Hotfix: 线上环境出bug，从master开一个hotfix，进行修复，修复完成后，合并回master和dev分支
* Release: dev分支准备好后，合并到release，经过最终测试并修改，通过测试后，合并回master和dev
* Feature: 开发新功能，从dev来，最终合并到dev去

首先，项目存在两个长期分支。
* 主分支master
* 开发分支dev
  前者用于存放对外发布的版本，任何时候在这个分支拿到的，都是稳定的分布版；后者用于日常开发，存放最新的开发版。

其次，项目存在三种短期分支。
* 功能分支（feature branch）
* 补丁分支（hotfix branch）
* 预发分支（release branch）
  一旦完成开发，它们就会被合并进dev或master，然后被删除。

![img.png](https://image-albatross.ddzuwu.com/f9/2023092239954ba8bda9f2635bb00cca28f58abb.png)

## 扩展包使用情况

| 扩展包 | 描述                  | 本项目应用场景                    |
| --- |---------------------|----------------------------|
| [guzzlehttp/guzzle](https://github.com/guzzle/guzzle) | HTTP 请求套件           | 请求三方服务 API                 |
| [godruoyi/php-snowflake](https://github.com/godruoyi/php-snowflake) | 雪花算法的 PHP 实现        | 用于生成风控订单编号                 |
| [predis/predis](https://github.com/nrk/predis.git) | Redis 官方首推的 PHP 客户端开发包 | 缓存驱动 Redis 基础扩展包           |
| [iidestiny/laravel-filesystem-oss](https://github.com/iiDestiny/laravel-filesystem-oss.git) | 阿里云OSS对象存储          | 用于存储上传图片及其他文件              |
| [overtrue/laravel-query-logger](https://github.com/overtrue/laravel-query-logger.git) | Laravel查询日志记录       | 用于记录 Laravel 应用程序所有查询的开发工具 |
| [php-open-source-saver/jwt-auth](https://github.com/PHP-Open-Source-Saver/jwt-auth.git) | Laravel 的 JSON Web 令牌身份验证 | 用于风控管理后台用户身份登录验证           |
| [phpoffice/phpword](https://github.com/PHPOffice/PHPWord.git) | 读写文字处理文档的纯 PHP 库 | 用于自动生成用户协议文档               |
| [tecnickcom/tcpdf](https://github.com/tecnickcom/TCPDF.git) | PHP 库的官方克隆，用于生成 PDF 文档和条形码 | 用于生成 PDF 文档和条形码 |
| [chendujin/id-card-number](https://github.com/chendujin/id-card-number.git) | 中国大陆身份证号码验证库 | 用于校验中国大陆用户身份证号码有效性         |
| [chendujin/mobile](https://github.com/chendujin/mobile.git) | PHP版本的中国手机号码归属地查询 | 用于校验中国手机号码归属地及运营商          |


## 自定义 Artisan 命令

| 命令行名字 | 说明                                            | Cron         | 代码调用 |
| --- |-----------------------------------------------|--------------| --- |
| `orders:automatic_distribute ` | 商家订单自动派单任务                                    | 每一分钟         | 无 |

## 队列清单

| 名称               | 说明         | 调用时机    |
|------------------|------------|---------|
| 暂无               | 暂无         | 暂无      |

## 其他常用命令
```
php artisan config:cache # 配置信息缓存
php artisan route:cache # 路由缓存
php artisan optimize # 类映射加载优化
composer dumpautoload # 自动加载优化
```

## 行为规范
### 严禁在生产环境调试修改代码
在任何时候，禁止在生产环境编辑代码、调试行为。

### 不允许随意更新扩展包版本
虽然很大可能更新扩展包都是没有问题的，但是并不代表不会问题出现，因而更新扩展包要谨慎，假设的确是有需要更新扩展包，更新后必须跑一边单元测试，否则有可能会造成某些代码片段运行异常。

### 操作前请备份
不管是改动文件还是数据，为了确保操作安全性，避免事故，务必要备份。文件备份使用原文件名称、时间、备份关键词作为名称。
```
# 示例
cp demo.ini demo.ini_$(date +%Y-%m-%d_%H:%M:%S)_bak
```
