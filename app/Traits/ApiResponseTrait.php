<?php

namespace App\Traits;

use App\Constants\Http\Status;
use App\Constants\Http\ErrorCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use App\Exceptions\BusinessException;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponseTrait
{
    /**
     * 成功
     *
     * @param array|null $data
     * @param int $code
     * @param string $message
     * @param array $headers
     * @return JsonResponse
     */
    public function success(?array $data = [], int $code = ErrorCode::SUCCESS, string $message = '', array $headers = []): JsonResponse
    {
        return $this->jsonResponse(true, $code, $data, $message, $headers);
    }

    /**
     * 失败
     *
     * @param int $code
     * @param string $message
     * @param array|null $data
     * @param array $headers
     * @param int $httpStatus
     * @return JsonResponse
     */
    public function fail(int $code = ErrorCode::FAIL, string $message = '', ?array $data = [], array $headers = [], int $httpStatus = Status::CODE_200): JsonResponse
    {
        return $this->jsonResponse(false, $code, $data, $message, $headers, $httpStatus);
    }

    /**
     * json响应
     *
     * @param bool $success
     * @param int $code
     * @param array $data
     * @param string $message
     * @param array $headers
     * @param int $httpStatus
     * @return JsonResponse
     */
    private function jsonResponse(bool $success, int $code, array $data, string $message, array $headers, int $httpStatus = Status::CODE_200): JsonResponse
    {
        $headers = $headers ? array_merge($headers, ['content-type' => 'application/json']) : ['content-type' => 'application/json'];

        return response()->json([
            'code' => $code,
            'msg' => $message ?: ErrorCode::getMessage($code),
            'data' => $data,
            'success' => $success,
            'trace_id' => request()->input('traceId', ''),
        ], $httpStatus)->withHeaders($headers);
    }

    /**
     * 成功分页返回
     *
     * @param mixed $page
     * @return JsonResponse
     */
    protected function successPaginate(mixed $page): JsonResponse
    {
        return $this->success($this->paginate($page));
    }

    /**
     * 分页处理
     *
     * @param mixed $page
     * @return mixed
     */
    private function paginate(mixed $page): mixed
    {
        if ($page instanceof LengthAwarePaginator) {
            return [
                'total' => $page->total(), // 数据总量
                'page' => $page->currentPage(), // 当前页数
                'page_size' => $page->perPage(), // 当前条数
                'pages' => $page->lastPage(), // 总页数
                'list' => $page->items(), // 数据列表
            ];
        }
        if ($page instanceof Collection) {
            $page = $page->toArray();
        }
        if (!is_array($page)) {
            return $page;
        }
        $total = count($page);

        return [
            'total' => $total,
            'page' => 1,
            'page_size' => $total,
            'pages' => 1,
            'list' => $page,
        ];
    }

    /**
     * 业务异常返回
     *
     * @param int $code
     * @param string $message
     *
     * @throws BusinessException
     */
    public function throwBusinessException(int $code = ErrorCode::FAIL, string $message = ''): void
    {
        throw new BusinessException($code, $message);
    }
}
