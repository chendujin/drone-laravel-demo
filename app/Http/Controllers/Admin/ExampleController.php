<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Repositories\ExampleRepository;
use App\Http\Controllers\BaseController;
use App\Http\Requests\Admin\ExampleRequest;

class ExampleController extends BaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->successPaginate(ExampleRepository::getList($request->all()));
    }

    /**
     * @param ExampleRequest $request
     * @return JsonResponse
     */
    public function store(ExampleRequest $request): JsonResponse
    {
        return $this->success([], message: '添加策略信息成功');
    }

    /**
     * @return JsonResponse
     */
    public function show(): JsonResponse
    {
        return $this->success([
            'hostname' => gethostname(),
            'ip' => gethostbyname(gethostname()),
        ]);
    }

    /**
     * @param ExampleRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(ExampleRequest $request, int $id): JsonResponse
    {
        return $this->success(message: '更新策略信息成功');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function selects(Request $request): JsonResponse
    {
        return $this->success([]);
    }
}
