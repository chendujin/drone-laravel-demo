<?php

namespace App\Models\Admin;

use Illuminate\Support\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\DatabaseNotification;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\DatabaseNotificationCollection;

/**
 * App\Models\Admin\Admin
 *
 * @property int $id
 * @property string $email 邮箱
 * @property string $name 登录账号
 * @property string $realname 用户名称
 * @property int $supplier_id 供应商ID
 * @property string $shalou_key 鲨漏验机客户端编号
 * @property string $password 登录密码
 * @property int $administrator 是否超管，1是，0否
 * @property int $merchant_id 商户id
 * @property int $level 权限级别,1:系统用户,2:中控用户,3:商家用户
 * @property string $remark 备注
 * @property int $status 状态，1启用，0禁用
 * @property int $is_online 上下线状态，1上线，0下线
 * @property int $creator_id 创建者id
 * @property Carbon|null $deleted_at 删除时间
 * @property string|null $last_login_at 最后登录时间
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read DatabaseNotification[]|DatabaseNotificationCollection $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection<int, Position> $positions
 * @property-read int|null $positions_count
 * @property-read Collection<int, Struct> $structs
 * @property-read int|null $structs_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Query\Builder|Admin onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereAdministrator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereIsOnline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLastLoginAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereMerchantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Admin withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Admin withoutTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRealname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereRemark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereShalouKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereSupplierId($value)
 *
 * @mixin \Eloquent
 */
class Admin extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'realname',
        'shalou_key',
        'supplier_id',
        'administrator',
        'level',
        'is_online',
        'creator_id',
        'remark',
        'status',
        'last_login_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
        'updated_at' => 'datetime:Y-m-d',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier(): mixed
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    // public function roles(): BelongsToMany
    // {
    //     return $this->belongsToMany(Role::class, AdminRole::class, 'admin_id', 'role_id');
    // }
    //
    // public function positions(): BelongsToMany
    // {
    //     // return $this->belongsTo(Position::class, 'position_id', 'id')->withDefault();
    //     return $this->belongsToMany(Position::class, AdminPosition::class, 'admin_id', 'position_id');
    // }
    //
    // public function structs(): BelongsToMany
    // {
    //     // return $this->belongsTo(Struct::class, 'struct_id', 'id')->withDefault();
    //     return $this->belongsToMany(Struct::class, AdminStruct::class, 'admin_id', 'struct_id');
    // }
}
