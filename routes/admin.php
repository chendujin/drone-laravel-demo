<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\ExampleController;

// Index控制器
Route::controller(ExampleController::class)->prefix('example')->name('example.')->group(function () {
    Route::get('/show', 'show')->name('show');
    Route::get('/index', 'index')->name('index');
});

// 后台管理路由
Route::middleware(['jwt.auth', 'admin.auth'])->name('admin.')->group(function () {
    // // 风控策略
    // Route::apiResource('risk-strategies', RiskStrategyController::class)->except('destroy');
    // // 风控策略枚举
    // Route::get('risk-strategies/enum/selects', [RiskStrategyController::class, 'selects'])->name('risk-strategies.selects');
});
